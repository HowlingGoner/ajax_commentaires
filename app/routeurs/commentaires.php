<?php
/*
  ./app/routeurs/commentaires.php
 */

include '../app/controleurs/commentairesControleur.php';

switch ($_GET['commentaire']) {
  case 'edit' :
  \App\Controleurs\CommentairesControleur\editAction($connexion, $_POST['commentaire'], $_POST['idCommentaire']);
  break;
  case 'delete' :
  App\Controleurs\CommentairesControleur\deleteAction($connexion, $_GET['id']);
  break;
  case 'add' :
  \App\Controleurs\CommentairesControleur\addAction($connexion, $_POST['pseudo'], $_POST['commentaire']);

  default :
  break;

}
