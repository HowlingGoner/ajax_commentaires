<?php
/*
    ./app/controleurs/commentairesControleur.php
 */
namespace App\Controleurs\CommentairesControleur;
use \App\Modeles\CommentairesModele AS Commentaire;

function indexAction(\PDO $connexion) {
  // Je vais chercher la liste des commentaires
    include '../app/modeles/commentairesModele.php';
    $commentaires = Commentaire\findAll($connexion);

  // Je charge la vue index dans $content1
    GLOBAL $content1, $title;
    $title = COMMENTAIRES_INDEX_TITLE;
    ob_start();
      include '../app/vues/commentaires/index.php';
    $content1 = ob_get_clean();
}

function addAction (\PDO $connexion, string $pseudo, string $commentaire) {
  // Je vais chercher le commentaire AJAX
  include '../app/modeles/commentairesModele.php';
  $id = Commentaire\addOne($connexion, $pseudo, $commentaire);
  include '../app/vues/commentaires/add.php';
}

function editAction (\PDO $connexion, string $commentaire, int $idCommentaire){
  include '../app/modeles/CommentairesModele.php';
  Commentaire\editOne($connexion, $commentaire, $idCommentaire);
}

function deleteAction(\PDO $connexion, int $idCommentaire) {
  include '../app/modeles/commentairesModele.php';
  echo Commentaire\deleteOne($connexion, $idCommentaire);

}
