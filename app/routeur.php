<?php
/*
  ./app/routeur.php
 */

 if (isset($_GET['commentaire'])) :
  include '../app/routeurs/commentaires.php';
  /*
    ROUTE PAR DEFAUT
    PATTERN: /
    CTRL: commentairesControleur
    ACTION: index
   */
else :
  include '../app/controleurs/commentairesControleur.php';
  \App\Controleurs\CommentairesControleur\indexAction($connexion);

endif;
