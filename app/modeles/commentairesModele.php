<?php
/*
    ./app/modeles/commentairesModele.php
 */
namespace App\Modeles\CommentairesModele;

/**
 * [findAll description]
 * @param  PDO   $connexion [description]
 * @return array            [description]
 */
function findAll(\PDO $connexion) :array {
  $sql = "SELECT *
          FROM commentaires
          ORDER BY created_at DESC;";

  $rs = $connexion->query($sql);
  return $rs->fetchAll(\PDO::FETCH_ASSOC);
}

function addOne(\PDO $connexion, string $pseudo,  string $commentaire){
  $sql = 'INSERT INTO commentaires
          SET pseudo = :pseudo,
            texte = :texte,
            created_at = NOW();';
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':pseudo', $pseudo, \PDO::PARAM_STR);
  $rs->bindValue(':texte', $commentaire, \PDO::PARAM_STR);
  $rs->execute();
  return $connexion->lastInsertId();
}

function editOne(\PDO $connexion, string $commentaire, int $idCommentaire) {
  $sql = 'UPDATE commentaires
          SET texte = :commentaire
          WHERE id= :idCommentaire;';
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':commentaire', $commentaire, \PDO::PARAM_STR);
  $rs->bindValue(':idCommentaire', $idCommentaire, \PDO::PARAM_INT);
  $rs->execute();
  }

function deleteOne(\PDO $connexion, int $idCommentaire) :bool {
  $sql = 'DELETE FROM
          commentaires
          WHERE id = :id;';
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $idCommentaire);
  return $rs->execute();
}
