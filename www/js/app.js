/*
  ./www/js/app.js
 */

$(function(){
  $('#form_commentaires').submit(function(e){
    e.preventDefault();
    $.ajax({
      url: 'commentaire/add',
      data: {
        pseudo : $("#pseudo").val(),
        commentaire : $("#Addcommentaire").val()
      },
      method: 'post',
    })
    .done(function(responsePHP) {
      //if(responsePHP > 0) {
        $('#listeDesPosts').prepend(responsePHP)
                           .find('li:first-of-type').hide().slideDown(300, function(){
                          // Vider les champs
                          $("#pseudo").val('');
                          $("#Addcommentaire").val('');
                        });;
      //}
    }).fail(function (){
      alert("Probleme durant la transaction !");
    })
  });

  $('#listeDesPosts').on('click', '.edit', function(){
    let text = $(this).parent().siblings('.text').text();
    let code = `<input id="input" type="text" value="${text}"/>`
    $(this).parent().siblings('.text').html(code);
    $(this).removeClass('edit').addClass('validate').text('Valider la modification');
  });

  $('#listeDesPosts').on('click', '.validate', function(){
     let idCommentaire = $(this).parents('li').attr('data-id');
     let commentaire = $("#input").val();
    $.ajax({
      url: 'commentaire/edit',
      data: {
        commentaire : commentaire,
        idCommentaire:  idCommentaire
      },
      method: 'post',
      context : this
    })
    .done(function(responsePHP) {
      alert(responsePHP)
      $('#input').remove();
      $(this).parent().siblings('.text').text(commentaire);
      $(this).removeClass('validate').addClass('edit').text('Editer le texte');

    }).fail(function (){
      alert("Probleme durant la transaction !");
    })
  });

  $('#listeDesPosts').on('click', '.delete', function(){
    let idCommentaire = $(this).parents('li').attr('data-id');
    $.ajax({
      url: 'commentaire/delete/' + idCommentaire,
      context : this
    })
    .done(function(responsePHP){
      if (responsePHP == 1) {
        $(this).closest('.post').slideUp(300, function(){
          $(this).remove();
        });
      }
    })
    .fail(function (){
        alert("Probleme durant la transaction !");
    })
  });

});
